
VERSION=0.1
DISTDIR=sugar-$(VERSION)
DISTBALL=$(DISTDIR).tar.gz

all: hello-sugar

sugar-gtk.h: mkwrap.py generate.py gtk.defs gtkenums.defs
	./mkwrap.py

hello-sugar: hello-sugar.cc sugar-gtk.h
	c++ -Wall -ggdb -O2 `gtk-config --libs --cflags` hello-sugar.cc -o hello-sugar

dist: sugar-gtk.h
	-mkdir $(DISTDIR)
	-mkdir $(DISTDIR)/doc
	-mkdir $(DISTDIR)/examples
	/bin/cp -f sugar-gtk.h $(DISTDIR)
	/bin/cp -f README $(DISTDIR)
	/bin/cp -f doc/mapping.txt $(DISTDIR)/doc
	/bin/cp -f hello-sugar.cc $(DISTDIR)/examples
	/bin/rm -f $(DISTBALL)
	tar cvfz $(DISTBALL) $(DISTDIR)/*

clean:
	/bin/rm -rf $(DISTDIR)
	/bin/rm -f sugar-gtk.h
	/bin/rm -f hello-sugar
	/bin/rm -f hello-sugar.o
	/bin/rm -f $(DISTBALL)
