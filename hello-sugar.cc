
// If you want to look at code size or that sort of thing you
// need to turn these on or you won't get accurate results.
#if 1
#define G_DISABLE_CHECKS
#define G_DISABLE_ASSERT
#define GTK_NO_CHECK_CASTS
#endif

#include "sugar-gtk.h"
#include <stdio.h>

using namespace Sugar;
using namespace Gtk;

class HelloDialog {
public:
  HelloDialog(Window& transient_parent);
  
private:
  Window dialog_;
  Label label_;
};

static void
dialog_destroy_cb(GtkDialog* dialog, gpointer data)
{
  HelloDialog* hd = static_cast<HelloDialog*>(data);

  delete hd;
}

HelloDialog::HelloDialog(Window& parent)
  : dialog_(Window::DIALOG), label_("Hello!")
{
  dialog_.add(label_);
  dialog_.connect("destroy", (SignalFunc)dialog_destroy_cb, this);
  dialog_.set_transient_for(parent);
  dialog_.show_all();
}

static void
button_clicked_cb(GtkButton* button, gpointer data)
{
  Label label(GTK_LABEL(data));

  label.set_text("Pushed!");
}

static void
hello_clicked_cb(GtkButton* button, gpointer data)
{
  Window w(GTK_WINDOW(data));

  new HelloDialog(w);
}

static gint
delete_event_cb(GtkWindow* w, Gdk::EventAny* event, gpointer data)
{
  main_quit();

  return TRUE;
}

int
main(int argc, char** argv)
{
  init(&argc, &argv);

  Window w(Window::TOPLEVEL);
  VBox vbox(false, 10);
  Button b1;
  Label l1("Push Me");
  Button b2("Hello World");
  
  b1.add(l1);

  b1.connect("clicked", (SignalFunc)button_clicked_cb,
             l1.gtk_label());

  b2.connect("clicked", (SignalFunc)hello_clicked_cb,
             w.gtk_window());
  
  vbox.pack_start(b1, true, true, 0);
  vbox.pack_start(b2, true, true, 0);
  
  w.add(vbox);

  w.connect("delete_event", (SignalFunc)delete_event_cb, 0);
  
  w.show_all();
  
  mainloop();
  
  return 0;
}


