#!/usr/bin/env python

import sys
import generate
from string import *
import re

TRUE = 1
FALSE = 0


retval_type_to_c_conversions = { 'int' : 'gint',
                                 'uint' : 'guint',
                                 'none' : 'void',
                                 'static_string' : 'const gchar*',
                                 'bool' : 'bool',
                                 'string' : 'gchar*',
                                 'shouldbeconst_string' : 'gchar*',
                                 'float' : 'gfloat',
                                 'double' : 'gdouble',
                                 'int*'  : 'gint*',
                                 'char***' : 'gchar***',
                                 'void*' : 'gpointer',
                                 'uint32' : 'guint32',
                                 'const_string' : 'const gchar*',
                                 'char' : 'gchar',
                                 'uchar' : 'guchar',
                                 'ushort' : 'gushort',
                                 'ulong' : 'gulong',
                                 'ushort*' : 'gushort*',
                                 'ulong*' : 'gulong*',
                                 'uchar*' : 'guchar*',
                                 'const_GdkColor*' : 'const Gdk::Color*',
                                 'const_GdkFont*' : 'const Gdk::Font*',
                                 'GList*' : 'GList*',
                                 'GDestroyNotify' : 'GDestroyNotify',
                                 'GdkWindow**' : 'Gdk::Window**',
                                 'GdkDragProtocol*' : 'Gdk::DragProtocol*',
                                 'const_GdkWChar*' : 'const Gdk::WChar*',
                                 'gchar**' : 'gchar**',
                                 'GdkAxisUse*' : 'Gdk::AxisUse*',
                                 'GdkWChar*' : 'Gdk::WChar*',
                                 'GdkModifierType*' : 'Gdk::ModifierType*',
                                 'double*' : 'gdouble*',
                                 'GdkBitmap**' : 'Gdk::Bitmap**',
                                 'GdkVisualType**' : 'Gdk::VisualType**',
                                 'guchar**' : 'guchar**',
                                 'gint**' : 'gint**',
                                 'uint32*' : 'guint32*',
                                 'gchar***' : 'gchar***',
                                 'GdkAtom*' : 'Gdk::Atom*',
                                 'GQuark' : 'GQuark',
                                 'gpointer*' : 'gpointer*',
                                 'GtkArgInfo**' : 'Gtk::ArgInfo**',
                                 'va_list' : 'va_list',
                                 'GtkObjectClass*' : 'Gtk::Object::Class*',
                                 'GSList**' : 'GSList**',
                                 'uint*' : 'guint*',
                                 'GtkType*' : 'Gtk::Type*',
                                 'uint32**' : 'guint32**',
                                 'GtkWidget**' : 'GtkWidget**',
                                 'GNode*' : 'GNode*',
                                 'GdkPixmap**' : 'Gdk::Pixmap**',
                                 'float*' : 'gfloat*',
                                 'uint16' : 'guint16',
                                 'int16' : 'gint16',
                                 'GtkPackType*' : 'Gtk::PackType*',
                                 'GHashTable*' : 'GHashTable*',

                                 'uint8*' : 'guint8*',
                                 'uint8' : 'guint8',
                                 'GdkImage**' : 'Gdk::Image**',
                                 'GScanner*' : 'GScanner*',
                                 'char**' : 'gchar**',
                                 'GSList*' : 'GSList*',
                                 'gconstpointer' : 'const void*',
                                 'const_GtkTargetEntry*' : 'const Gtk::TargetEntry*',
                                 'const_guchar*' : 'const guchar*',
                                 'GtkFlagValue*' : 'Gtk::FlagValue*',
                                 'const_GtkTypeInfo*' : 'const Gtk::TypeInfo*',

                                 'GCompareFunc' : 'GCompareFunc',
                                 'const_GtkArg*' : 'const Gtk::Arg*',
                                 'GtkPathPriorityType*' : 'Gtk::PathPriorityType*',
                                 'GtkStateType*' : 'Gtk::StateType*',
                                 # Gtk typo
                                 'GtkSelectioData*' : 'Gtk::SelectionData*'
                                 }

# in an argument list
arg_type_to_c_conversions = { 'int' : 'gint',
                              'uint' : 'guint',
                              'none' : 'void',
                              'static_string' : 'const gchar*',
                              'bool' : 'bool',
                              'string' : 'const gchar*',
                              'shouldbeconst_string' : 'const gchar*',
                              'float' : 'gfloat',
                              'double' : 'gdouble',
                              'int*'  : 'gint*',
                              'char***' : 'gchar***',
                              'void*' : 'gpointer',
                              'uint32' : 'guint32',
                              'const_string' : 'const gchar*',
                              'char' : 'gchar',
                              'uchar' : 'guchar',
                              'ushort' : 'gushort',
                              'ulong' : 'gulong',
                              'ushort*' : 'gushort*',
                              'ulong*' : 'gulong*',
                              'uchar*' : 'guchar*',
                              'bool*' : 'gboolean*',
                              'const_GdkColor*' : 'const Gdk::Color*',
                              'const_GdkFont*' : 'const Gdk::Font*',
                              'GList*' : 'GList*',
                              'GDestroyNotify' : 'GDestroyNotify',
                              'GdkWindow**' : 'Gdk::Window**',
                              'GdkDragProtocol*' : 'Gdk::DragProtocol*',
                              'const_GdkWChar*' : 'const Gdk::WChar*',
                              'gchar**' : 'gchar**',
                              'GdkAxisUse*' : 'Gdk::AxisUse*',
                              'GdkWChar*' : 'Gdk::WChar*',
                              'GdkModifierType*' : 'Gdk::ModifierType*',
                              'double*' : 'gdouble*',
                              'GdkBitmap**' : 'Gdk::Bitmap**',
                              'GdkVisualType**' : 'Gdk::VisualType**',
                              'guchar**' : 'guchar**',
                              'gint**' : 'gint**',
                              'uint32*' : 'guint32*',
                              'gchar***' : 'gchar***',
                              'GdkAtom*' : 'Gdk::Atom*',
                              'GQuark' : 'GQuark',
                              'gpointer*' : 'gpointer*',
                              'GtkArgInfo**' : 'Gtk::ArgInfo**',
                              'va_list' : 'va_list',
                              'GtkObjectClass*' : 'Gtk::Object::Class*',
                              'GSList**' : 'GSList**',
                              'uint*' : 'guint*',
                              'GtkType*' : 'Gtk::Type*',
                              'uint32**' : 'guint32**',
                              'GtkWidget**' : 'GtkWidget**',
                              'GNode*' : 'GNode*',
                              'GdkPixmap**' : 'Gdk::Pixmap**',
                              'float*' : 'gfloat*',
                              'uint16' : 'guint16',
                              'int16' : 'gint16',
                              'GtkPackType*' : 'Gtk::PackType*',
                              'GHashTable*' : 'GHashTable*',

                              'uint8*' : 'guint8*',
                              'uint8' : 'guint8',
                              'GdkImage**' : 'Gdk::Image**',
                              'GScanner*' : 'GScanner*',
                              'char**' : 'gchar**',
                              'GSList*' : 'GSList*',
                              'gconstpointer' : 'const void*',
                              'const_GtkTargetEntry*' : 'const Gtk::TargetEntry*',
                              'const_guchar*' : 'const guchar*',
                              'GtkFlagValue*' : 'Gtk::FlagValue*',
                              'const_GtkTypeInfo*' : 'const Gtk::TypeInfo*',

                              'GCompareFunc' : 'GCompareFunc',
                              'const_GtkArg*' : 'const Gtk::Arg*',
                              'GtkPathPriorityType*' : 'Gtk::PathPriorityType*',
                              'GtkStateType*' : 'Gtk::StateType*',
                              # Gtk typo
                              'GtkSelectioData*' : 'Gtk::SelectionData*'
                              }


def scopize_string(str, prefix, current_namespace = None):
    if prefix[0:len(current_namespace)] == current_namespace:
        prefix = prefix[len(current_namespace):]
    # stupid hack
    elif prefix == 'GdkRgb':
        return 'Gdk::Rgb::' + str
    if prefix != '':
        return prefix + '::' + str
    else:
        return str

def convert_typename(name, current_namespace = None):
    if retval_type_to_c_conversions.has_key(name):
        return retval_type_to_c_conversions[name]
    elif generate.objects.has_key(name):
        return generate.objects[name].get_nv().get_without_prefix()
    elif generate.enums.has_key(name):
        nv = generate.enums[name].get_nv()
        if current_namespace != None:
            return scopize_string(nv.get_without_prefix(), nv.get_prefix(), current_namespace)
        else:
            return nv.get_without_prefix()

    elif generate.flags.has_key(name):
        # in C++ the result of BLAH | FOO is not the enum containing BLAH, FOO
        return 'int' # generate.flags[name].get_nv().get_without_prefix()
    elif generate.boxeds.has_key(name):
        nv = generate.boxeds[name].get_nv()
        if current_namespace != None:
            return scopize_string(nv.get_without_prefix(), nv.get_prefix(), current_namespace) + '*'
        else:
            return nv.get_prefix() + '::' + nv.get_without_prefix() + '*'
    elif generate.typedefs.has_key(name):
        nv = generate.typedefs[name].get_nv()
        if current_namespace != None:
            return scopize_string(nv.get_without_prefix(), nv.get_prefix(), current_namespace)
        else:
            return nv.get_prefix() + '::' + nv.get_without_prefix()
    else:
        print "Warning: no C equivalent of `%s'" % name
        return name

def convert_arg_typename(name, current_namespace = None):
    if arg_type_to_c_conversions.has_key(name):
        return arg_type_to_c_conversions[name]
    elif generate.objects.has_key(name):
        return generate.objects[name].get_nv().get_without_prefix() + '&'
    elif generate.enums.has_key(name):
        nv = generate.enums[name].get_nv()
        if current_namespace != None:
            return scopize_string(nv.get_without_prefix(), nv.get_prefix(), current_namespace)
        else:
            return nv.get_without_prefix()

    elif generate.flags.has_key(name):
        # in C++ the result of BLAH | FOO is not the enum containing BLAH, FOO
        return 'int'
    #        nv = generate.flags[name].get_nv()
    #        return nv.get_without_prefix()
    elif generate.boxeds.has_key(name):
        nv = generate.boxeds[name].get_nv()
        if current_namespace != None:
            return scopize_string(nv.get_without_prefix(), nv.get_prefix(), current_namespace) + '*'
        else:
            return nv.get_prefix() + '::' + nv.get_without_prefix() + '*'
    elif generate.typedefs.has_key(name):
        nv = generate.typedefs[name].get_nv()
        if current_namespace != None:
            return scopize_string(nv.get_without_prefix(), nv.get_prefix(), current_namespace)
        else:
            return nv.get_prefix() + '::' + nv.get_without_prefix()
    else:
        print "Warning: no C equivalent of `%s'" % name
        return name

def write_enums_for_prefix(prefix):
    # Generate enumerations; these work like this:
    #  typedef enum
    #  {
    #    ARROW_UP = GTK_ARROW_UP,
    #    ARROW_DOWN = GTK_ARROW_DOWN,
    #    ARROW_LEFT = GTK_ARROW_LEFT,
    #    ARROW_RIGHT = GTK_ARROW_RIGHT
    #  } ArrowType;

    for (name, enum) in generate.enums.items():
        nv = enum.get_nv()
        if nv.get_prefix() == prefix:
            outfile.write('  typedef enum\n  {\n')
            for v in enum.get_values():
                nv2 = v.get_nv()
                valname = nv2.get_cast_without_prefix()
                # 2BUTTON_PRESS isn't a valid identifier
                if valname[0] in digits:
                    valname = '_' + valname
                outfile.write('    ' + valname + ' = ' + nv2.get_cast() + ',\n')
            outfile.write('  } ' + nv.get_without_prefix() + ';\n\n')

def write_flags_for_prefix(prefix):
    # Just like generating enums 
    for (name, flag) in generate.flags.items():
        nv = flag.get_nv()
        if nv.get_prefix() == prefix:
            outfile.write('  typedef enum\n  {\n')
            for v in flag.get_values():
                nv2 = v.get_nv()
                valname = nv2.get_cast_without_prefix()

                # 2BUTTON_PRESS isn't a valid identifier
                if valname[0] in digits:
                    valname = '_' + valname
                outfile.write('    ' + valname + ' = ' + nv2.get_cast() + ',\n')
            outfile.write('  } ' + nv.get_without_prefix() + ';\n\n')

def write_boxeds_for_prefix(prefix):
    # typedef boxed values into the prefix namespace
    def sortfunc(tup1, tup2):
        return cmp(tup1[0], tup2[0])
    
    boxeds =  generate.boxeds.items();
    boxeds.sort(sortfunc)

    for (name, boxed) in boxeds:
        nv = boxed.get_nv()
        if nv.get_prefix() == prefix:
            outfile.write('  typedef ' + name + ' ' + nv.get_without_prefix() + ';\n')
    outfile.write('\n')

def write_typedefs_for_prefix(prefix):
    # typedef typedef types into the prefix namespace
    def sortfunc(tup1, tup2):
        return cmp(tup1[0], tup2[0])
    
    tds =  generate.typedefs.items();
    tds.sort(sortfunc)

    for (name, td) in tds:
        nv = td.get_nv()
        if nv.get_prefix() == prefix:
            outfile.write('  typedef ' + name + ' ' + nv.get_without_prefix() + ';\n')
    outfile.write('\n')

strip_stars_regexp = re.compile("([A-Za-z0-9_]+)(\**)")

def generate_arglist(m, is_method, with_types, current_namespace):
    argstring = '('
    list = None
    if is_method:
        list = (m.get_args())[1:]  # omit first arg
    else:
        list = m.get_args()

    # first arg is the function to access the C object
    # if this is a method
    if is_method and not with_types:
        method_of = m.get_method_of()
        if method_of == None:
            print "Warning: %s is not a method?" % m.get_name()
        argstring = argstring + method_of.get_nv().get_function_prefix() + '(), '

    for a in list:
        if with_types:
            argstring = argstring + convert_arg_typename(a[0], current_namespace) + ' '

        if with_types or not generate.objects.has_key(a[0]):
            cast = ''
            close_cast = ''
            if not with_types:
                mo = strip_stars_regexp.match(a[0])
                if mo == None:
                    print "Failed to match star-stripping regexp"
                    sys.exit(1)
                base = mo.group(1)
                stars = mo.group(2)
                if generate.enums.has_key(base):
                    nv = generate.enums[base].get_nv()
                    cast = '(' + nv.get_basename() + stars + ')'
                elif generate.flags.has_key(base):
                    nv = generate.flags[base].get_nv()
                    cast = '(' + nv.get_basename() + stars + ')'
                elif a[0] == 'shouldbeconst_string':
                    cast = 'const_cast<gchar*>('
                    close_cast = ')'
                    
            argstring = argstring + cast + a[1] + close_cast + ', '
        else:
            # have to access the C object
            argstring = argstring + a[1] + '.' + generate.objects[a[0]].get_nv().get_function_prefix() + '(), '
                
    if argstring == '(':
        argstring = '()'
    else:
        argstring = argstring[:-2] + ')'
    return argstring

def get_return_cast(rettype):
    retcast = ''
    if generate.enums.has_key(rettype):
        nv = generate.enums[rettype].get_nv()
        retcast = '(' + scopize_string(nv.get_without_prefix(), nv.get_prefix(), "Gtk") + ')'
    elif generate.flags.has_key(rettype):
        nv = generate.flags[rettype].get_nv()
        retcast = '(' + scopize_string(nv.get_without_prefix(), nv.get_prefix(), "Gtk") + ')'
    elif rettype == 'bool':
        # canonicalize the bool
        retcast = '!!'

    return retcast

def write_funcs_for_prefix(prefix, current_namespace):
    fprefix = lower(prefix)
    # more hacks!
    if fprefix == 'gdkrgb':
        fprefix = 'gdk_rgb'

    def sortfunc(f1, f2):
        return cmp(f1.get_name(), f2.get_name())

    funcs = generate.functions.values()
    funcs.sort(sortfunc)

    for f in funcs:
        # filter members
        if f.is_member():
            continue
        elif (f.get_name())[0:len(fprefix)] != fprefix:
            continue
        else:
            fname = (f.get_name())[len(fprefix)+1:]
            rettype = convert_typename(f.get_rettype(), current_namespace)
            # Gtk::main() has to be explicitly distinguished from main()
            if fname == 'main':
                fname = 'mainloop'
            # keywords
            elif fname == 'false':
                fname = 'gtk_false'
            elif fname == 'true':
                fname = 'gtk_true'

            outfile.write('  static inline ' + rettype + ' ' + fname + ' ')
            argstring = generate_arglist(f, FALSE, TRUE, current_namespace)
            outfile.write(argstring + ' {\n')
            argstring = generate_arglist(f, FALSE, FALSE, current_namespace)
            ret = ''
            if rettype != 'void':
                ret = 'return '
            outfile.write('    ' + ret +  get_return_cast(f.get_rettype()) + f.get_name() + ' ' + argstring + ';\n')
            outfile.write('  }\n\n')


def write_copyright(filehandle):
  filehandle.write("""
/*

  Sugar is a C++ wrapper for GTK+ and GNOME developed by
  Havoc Pennington <hp@redhat.com> using code developed
  by James Henstridge and others for the GNOME Python bindings.

  This file is automatically generated from some scripts, and forms
  one part of the complete Sugar distribution. This file is Copyright
  (C) 2000 Red Hat, Inc. and may be used under the following terms:

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE XFREE86 PROJECT BE LIABLE FOR ANY CLAIM, DAMAGES
    OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
    THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  The scripts and data files used to generate this file fall under a
  separate license; see those files for details.
    
*/

""")


#############################

p = generate.FilteringParser(input='gtk.defs',
			     prefix='gtkmodule')

p.startParsing()

p.associate_methods()

outfile = open('sugar-gtk.h', 'w')

outfile.write("// -*- C++ -*-\n")

outfile.write("""
#ifndef SUGAR_GTK_H
#define SUGAR_GTK_H
""")

write_copyright(outfile)

outfile.write("""

#include <gdk/gdk.h>

namespace Sugar {
namespace Gdk {
""")


# RGB types (types before functions)
outfile.write("""
  namespace Rgb {
""")

write_enums_for_prefix('GdkRgb')
write_flags_for_prefix('GdkRgb')
write_boxeds_for_prefix('GdkRgb')
write_typedefs_for_prefix('GdkRgb')

outfile.write("""
  }; // namespace Rgb
  
""")

write_enums_for_prefix('Gdk')
write_flags_for_prefix('Gdk')
# outfile.write('namespace GC {\n')
write_enums_for_prefix('GdkGC')
write_flags_for_prefix('GdkGC')
# outfile.write('} // namespace GC\n\n')
write_boxeds_for_prefix('Gdk')
write_typedefs_for_prefix('Gdk')

write_funcs_for_prefix('Gdk', 'Gdk')

# RGB functions
outfile.write("""
  namespace Rgb {
""")

write_funcs_for_prefix('GdkRgb', 'Gdk::Rgb')

outfile.write("""
  }; // namespace Rgb
  
""")

outfile.write("""
};   // namespace Gdk
};   // namespace Sugar
""")

outfile.write("""
#include <gtk/gtk.h>

namespace Sugar {
namespace Gtk {

""")

write_enums_for_prefix('Gtk')
write_flags_for_prefix('Gtk')
write_boxeds_for_prefix('Gtk')
write_typedefs_for_prefix('Gtk')

gtkobject = generate.objects["GtkObject"]

already_defined = {}
def define_object(name, obj):    
    if already_defined.has_key(name):
        return
    else:
        already_defined[name] = obj

    parent = obj.get_parent()
    parent_name = ''
    if parent != None:
        parent_name = parent.get_nv().get_basename()

    # Define the object's parent if it hasn't been; keeps us in order
    if parent != None and not already_defined.has_key(parent_name):
        define_object(parent_name, parent)
    
    nv = obj.get_nv()
    outfile.write('  class ' + nv.get_without_prefix() )
    if parent != None:
        outfile.write(' : public ' + parent.get_nv().get_without_prefix())
    outfile.write(' {\n  public:\n')

    defined_void_constructor = FALSE

    # typedef for the class struct
    classname = nv.get_basename() + 'Class'
    outfile.write('    typedef ' + classname + ' Class;\n')

    # Write out the special-case GtkObject stuff 
    if obj == gtkobject:
        outfile.write("""        
    Object(GtkObject* obj) : object_(obj) {
      if (obj != 0)
        {
          gtk_object_ref(obj);
          gtk_object_sink(obj);
        }
    }

    ~Object() {
      if (object_ != 0)
        gtk_object_unref(object_);
    }

    Object(const Object& src) : object_(src.object_) {
      if (object_ != 0)
        gtk_object_ref(object_);
    }

    Object& operator=(const Object& src) {
      /* self-assignment */
      if (object_ == src.object_)
        return *this;

      if (object_ != 0)
        gtk_object_unref(object_);
      
      object_ = src.object_;

      if (object_ != 0)
        gtk_object_ref(object_);

      return *this;
    }
    
    bool is_null() { return object_ == 0; }

    void set_null() {
      if (object_ != 0)
        gtk_object_unref(object_);
      object_ = 0;
    }

    // drop our handle on the C object,
    // without decrementing the refcount
    // (useful if you want your widget to persist
    //  after its handle goes out of scope)
    void release() {
      object_ = 0;
    }

    GtkObject* gtk_object() { return object_; }

    private:
      GtkObject* object_;

    public:
    
""")
    else:
        if parent == None:
            print "Warning: object has no parent type", nv.get_basename()
            
        # from-C-object constructor
        outfile.write('    ' + nv.get_without_prefix() + ' (' + nv.get_basename() + '* ' + lower(nv.get_without_prefix()) + ') : ' + parent.get_nv().get_without_prefix() + '((' + parent.get_nv().get_basename() + '*)' + lower(nv.get_without_prefix()) + ') {\n' + '      g_return_if_fail(' + lower(nv.get_without_prefix()) + ' == 0 || ' + nv.get_type_check() + '(' + lower(nv.get_without_prefix()) + '));\n    }\n\n')

        # C-object accessor
        outfile.write('    ' + nv.get_basename() + '* ' + nv.get_function_prefix() + ' () { return ' + nv.get_cast() + ' (gtk_object()); }\n\n')

    # get_type

    outfile.write('    static Gtk::Type get_type() {\n' + '      return ' + nv.get_function_prefix() + '_get_type();\n    }\n\n')

    # write enums associated with this object 
    write_enums_for_prefix(nv.get_basename())
    # and flags
    write_flags_for_prefix(nv.get_basename())
    # and typedefs
    write_boxeds_for_prefix(nv.get_basename())
    write_typedefs_for_prefix(nv.get_basename())

    for c in obj.get_constructors():        
        argstring = generate_arglist(c, FALSE, TRUE, 'Gtk')
        argstring_notypes = generate_arglist(c, FALSE, FALSE, 'Gtk')
        if argstring == '()':
            defined_void_constructor = TRUE

        if parent != None:
            constructor = '    ' + nv.get_without_prefix() +  argstring + ' : ' + parent.get_nv().get_without_prefix() + '((' + parent.get_nv().get_basename() + '*)' + c.get_name() + argstring_notypes + ') {}\n\n'
        else:
            constructor = '    ' + nv.get_without_prefix() +  argstring + ' : object_((GtkObject*)' + c.get_name() + argstring_notypes + ') {\n          gtk_object_ref(object_);\n          gtk_object_sink(object_);\n        }\n\n'
            
        outfile.write(constructor)

    # Remember that some of the methods may be macros,
    # in generate.macros instead of generate.functions
    # and with uppercase names
    def msortfunc(m1, m2):
        return cmp(m1.get_name(), m2.get_name())
    methods = obj.get_methods()
    methods.sort(msortfunc)
    for m in methods:
        fprefix = nv.get_function_prefix()

        # special case to make gtk_signal_*(GtkObject*) a method
        # of GtkObject
        mname = ''
        if obj == gtkobject and (m.get_name())[0:len('gtk_signal')] == 'gtk_signal':
            mname = (m.get_name())[len('gtk_signal')+1:]
        else:
            mname = lower((m.get_name())[len(fprefix)+1:])

        rettype = convert_typename(m.get_rettype(), "Gtk")
        outfile.write('    ' + rettype + ' ' + mname + ' ')
        argstring = generate_arglist(m, TRUE, TRUE, 'Gtk')
        outfile.write(argstring + ' {\n')
        argstring = generate_arglist(m, TRUE, FALSE, 'Gtk')
        ret = ''
        if rettype != 'void':
            ret = 'return '
        outfile.write('      '+ ret + get_return_cast(m.get_rettype()) + m.get_name() + ' ' + argstring + ';\n')
        outfile.write('    }\n\n')

    def smsortfunc(m1, m2):
        return cmp(m1.get_name(), m2.get_name())
    statics = obj.get_static_members()
    statics.sort(smsortfunc)
    for sf in statics:
        fprefix = nv.get_function_prefix()
        mname = (sf.get_name())[len(fprefix)+1:]

        if mname == 'get_type': # filter these out
            continue
        
        rettype = convert_typename(sf.get_rettype(), "Gtk")
        outfile.write('    static ' + rettype + ' ' + mname + ' ')
        argstring = generate_arglist(sf, FALSE, TRUE, 'Gtk')
        outfile.write(argstring + ' {\n')
        argstring = generate_arglist(sf, FALSE, FALSE, 'Gtk')
        ret = ''
        if rettype != 'void':
            ret = 'return '
        outfile.write('      ' + ret +  get_return_cast(sf.get_rettype()) + sf.get_name() + ' ' + argstring + ';\n')
        outfile.write('    }\n\n')

    # Make accessors for fields
    for (field_type, field_name) in obj.get_fields():
        rettype = convert_typename(field_type, "Gtk")
        outfile.write('    ' + rettype + ' ' + 'get_' + field_name + ' () {\n')
        outfile.write('      return ' + get_return_cast(field_type) + nv.get_function_prefix() + '()->' + field_name + ';\n    }\n\n')

    # make it private
    if not defined_void_constructor:
        outfile.write('  private:\n    ' + nv.get_without_prefix() + ' (); // not implemented\n')

    outfile.write('\n  }; // ' + nv.get_without_prefix() + '\n\n')

# alphabetize the objects first, then define_object() imposes
# class-inheritance-ordering

def sortfunc(tup1, tup2):
    return cmp(tup1[0], tup2[0])

objs = generate.objects.items()
objs.sort(sortfunc)

# forward-declare all the classes
for (name, obj) in objs:
    outfile.write('  class ' + obj.get_nv().get_without_prefix() + ';\n')

outfile.write('\n\n')

# now define them, but special case a couple to get things
# in the right order
define_object('GtkObject', generate.objects['GtkObject'])
define_object('GtkAdjustment', generate.objects['GtkAdjustment'])

for (name, obj) in objs:
    define_object(name, obj)

# non-member functions 
write_funcs_for_prefix('Gtk', 'Gtk')

outfile.write("""

}; // namespace Gtk
}; // namespace Sugar
#endif // SUGAR_GTK_H
""")

