import os
import scmexpr
try:
	from cStringIO import StringIO
except ImportError:
	from StringIO import StringIO
# for fnmatchcase -- filename globbing
import fnmatch
import re
from string import *

TRUE  = 1
FALSE = 0

enums = {}
flags = {}
objects = {}
functions = {}
macros = {}
boxeds = {}
objects_by_prefix = {}
typedefs = {}

def underscore_join(list_of_str):
        newstr = ''
        no_uscore = TRUE
        for s in list_of_str:
                if not no_uscore:
                        newstr = newstr + '_'

                newstr = newstr + s
                
                if len(s) == 1:
                        no_uscore = TRUE
                else:
                        no_uscore = FALSE

        return newstr

class NameVariants:
        def __init__(self):
                self.basename = ""

        regexp_match_studlycaps_element = re.compile("([0-9A-Z]+[0-9a-z]*)")

        prefix_special_cases = { 'GtkCTreeNode' : 'GtkCTree',
                                 'GtkCTreePos' : 'GtkCTree',
                                 'GtkCTreeLineStyle' : 'GtkCTree',
                                 'GtkCTreeExpanderStyle' : 'GtkCTree',
                                 'GtkCTreeExpansionType' : 'GtkCTree',
                                 'GtkToolbarChildType' : 'GtkToolbar',
                                 'GtkTreeViewMode' : 'GtkTree',
                                 'GtkWindowType' : 'GtkWindow',
                                 'GtkButtonBoxStyle' : 'GtkButtonBox',
                                 'GtkCurveType' : 'GtkCurve',
                                 'GtkToolbarSpaceStyle' : 'GtkToolbar',
                                 'GtkArrowType' : 'GtkArrow',
                                 'GtkMenuDirectionType' : 'GtkMenu',
                                 'GtkSpinButtonUpdatePolicy' : 'GtkSpinButton',
                                 'GtkClistDragPos' : 'GtkCList',
                                 'GtkPreviewType' : 'GtkPreview',
                                 'GdkGCValuesMask' : 'GdkGC',
                                 'GtkWidgetFlags' : 'GtkWidget',
                                 'GtkObjectFlags' : 'GtkObject',
                                 'GtkProgressBarStyle' : 'GtkProgressBar',
                                 'GtkProgressBarOrientation' : 'GtkProgressBar',
                                 'GtkProgressLeftToRight' : 'GtkProgress',
                                 'GtkProgressRightToLeft' : 'GtkProgress',
                                 'GtkProgressTopToBottom' : 'GtkProgress',
                                 'GtkProgressBottomToTop' : 'GtkProgress',
                                 'GtkProgressContinuous' : 'GtkProgress',
                                 'GtkProgressDiscrete' : 'GtkProgress',
                                 'GdkRgbDither' : 'GdkRgb',
                                 'GdkRgbCmap' : 'GdkRgb'
                                 }

        elements_special_cases = { 'GdkXCursor' : # GDK_X_CURSOR
                                   ('Gdk', 'XCursor', # with/without prefix
                                    ('Gdk'), # prefix elements
                                    ('X', 'Cursor'),  # other elements
                                    'GDK', # cast prefix
                                    'X_CURSOR' # cast no prefix
                                    ),

                                   'GtkTypeCCallback' :
                                   ('Gtk', 'TypeCCallback', # with/without prefix
                                    ('Gtk'), # prefix elements
                                    ('Type', 'C', 'Callback'),  # other elements
                                    'GTK', # cast prefix
                                    'TYPE_C_CALLBACK' # cast no prefix
                                    ),

                                   'GdkSbHDoubleArrow' : # GDK_SB_H_DOUBLE_ARROW
                                   ('Gdk', 'SBHDoubleArrow', # with/without prefix
                                    ('Gdk'), # prefix elements
                                    ('SB', 'H', 'Double', 'Arrow'),  # other elements
                                    'GDK', # cast prefix
                                    'SB_H_DOUBLE_ARROW' # cast no prefix
                                    ),
                                  
                                   'GdkSbVDoubleArrow' : # GDK_SB_V_DOUBLE_ARROW
                                   ('Gdk', 'SBVDoubleArrow', # with/without prefix
                                    ('Gdk'), # prefix elements
                                    ('SB', 'V', 'Double', 'Arrow'),  # other elements
                                    'GDK', # cast prefix
                                    'SB_V_DOUBLE_ARROW' # cast no prefix
                                    ),

                                   'Gdk2ButtonPress' :
                                   ('Gdk', '2ButtonPress',
                                    ('Gdk'),
                                    ('2Button', 'Press'),
                                    'GDK',
                                    '2BUTTON_PRESS'),
                                   
                                   'Gdk3ButtonPress' :
                                   ('Gdk', '3ButtonPress',
                                    ('Gdk'),
                                    ('3Button', 'Press'),
                                    'GDK',
                                    '3BUTTON_PRESS'),
                                   
                                   'GdkGCTSXOrigin' :
                                   ('GdkGC', 'TSXOrigin',
                                    ('Gdk', 'GC'),
                                    ('TS', 'X', 'Origin'),
                                    'GDK_GC',
                                    'TS_X_ORIGIN'),

                                   'GdkGCTSYOrigin' :
                                   ('GdkGC', 'TSYOrigin',
                                    ('Gdk', 'GC'),
                                    ('TS', 'Y', 'Origin'),
                                    'GDK_GC',
                                    'TS_Y_ORIGIN'),

                                   'GdkGCClipXOrigin' :
                                   ('GdkGC', 'ClipXOrigin',
                                    ('Gdk', 'GC'),
                                    ('Clip', 'X', 'Origin'),
                                    'GDK_GC',
                                    'CLIP_X_ORIGIN'),

                                   'GdkGCClipYOrigin' :
                                   ('GdkGC', 'ClipYOrigin',
                                    ('Gdk', 'GC'),
                                    ('Clip', 'Y', 'Origin'),
                                    'GDK_GC',
                                    'CLIP_Y_ORIGIN')

                                   }

        broken_cast_names = { 'GTK_BUTTON_BOX_DEFAULT_STYLE' :
                              'GTK_BUTTONBOX_DEFAULT_STYLE',
                              'GTK_BUTTON_BOX_SPREAD' :
                              'GTK_BUTTONBOX_SPREAD',
                              'GTK_BUTTON_BOX_EDGE' :
                              'GTK_BUTTONBOX_EDGE',
                              'GTK_BUTTON_BOX_START' :
                              'GTK_BUTTONBOX_START',
                              'GTK_BUTTON_BOX_END' :
                              'GTK_BUTTONBOX_END'
                              }

        # Set from GtkBlahFoo
        def set_basename(self, name, prefix_from = None):
                self.basename = name

                self.cast_prefix = None
                self.no_prefix_cast = None

                if self.elements_special_cases.has_key(name):
                        tup = self.elements_special_cases[name]
                        self.prefix = tup[0]
                        self.no_prefix_name = tup[1]
                        self.basename = self.prefix + self.no_prefix_name
                        name = self.basename
                        self.prefix_elements = list(tup[2])
                        self.name_elements = list(tup[3])
                        self.cast_prefix = tup[4]
                        self.no_prefix_cast = tup[5]
                        
                else: # not a special case
                        mo = self.regexp_match_studlycaps_element.match(name)

                        remainder = ''
                        self.prefix = ''
                        
                        # bad hack to accomodate set_cast
                        prefix_from_name = name
                        if prefix_from != None:
                                prefix_from_name = prefix_from
                                
                        if self.prefix_special_cases.has_key(prefix_from_name):
                                self.prefix = self.prefix_special_cases[prefix_from_name]
                                remainder = name[len(self.prefix):]
                        else:
                                if mo != None:
                                        self.prefix = mo.group(0)
                                        remainder = mo.string[mo.end(0):]
                                else:
                                        print "Warning: couldn't get name variants for: ", name
                                        return

                        # I feel sure that regexp syntax has a way to do
                        # this automatically...
                        self.name_elements = []

                        self.no_prefix_name = remainder
                        
                        while remainder != "":
                                mo = self.regexp_match_studlycaps_element.match(remainder)

                                if mo != None:
                                        elem = mo.group(0)
                                        self.name_elements.append(elem)
                                        remainder = mo.string[mo.end(0):]
                                else:
                                        break

                        # now compute the prefix elements
                        self.prefix_elements = []
                        remainder = self.prefix
                        while remainder != '':
                                mo = self.regexp_match_studlycaps_element.match(remainder)
                                
                                if mo != None:
                                        elem = mo.group(0)
                                        self.prefix_elements.append(elem)
                                        remainder = mo.string[mo.end(0):]
                                else:
                                        break


                        
                lowercase_elements = map(lambda x: lower(x),
                                         [self.prefix] + self.name_elements)
                self.function_prefix = underscore_join(lowercase_elements)

                lowercase_end_elements =  map(lambda x: lower(x),
                                              self.name_elements)
                self.var_name = underscore_join(lowercase_end_elements)

                if self.cast_prefix == None:
                        uppercase_elements = map(lambda x: upper(x),
                                                 self.prefix_elements)
                        self.cast_prefix = underscore_join(uppercase_elements)

                if self.no_prefix_cast == None:
                        uppercase_elements = map(lambda x: upper(x),
                                                 self.name_elements)
                        
                        self.no_prefix_cast = underscore_join(uppercase_elements)

                self.cast = self.cast_prefix + '_' + self.no_prefix_cast

                if self.broken_cast_names.has_key(self.cast):
                        self.cast = self.broken_cast_names[self.cast]

                uppercase_with_IS = map(lambda x: upper(x),
                                        [self.prefix] + ['IS'] + self.name_elements)
                self.typecheck = underscore_join(uppercase_with_IS)

                uppercase_with_TYPE =  map(lambda x: upper(x),
                                           [self.prefix] + ['TYPE'] + self.name_elements)
                self.typeid = underscore_join(uppercase_with_TYPE)

                self.classname = self.basename + "Class"

                uppercase_with_CLASS = map(lambda x: upper(x),
                                           [self.prefix] + self.name_elements + ['CLASS'])
                self.classcast = underscore_join(uppercase_with_CLASS)

                uppercase_with_IS_CLASS = map(lambda x: upper(x),
                                              [self.prefix] + ['IS'] +
                                              self.name_elements + ['CLASS'])
                self.classtypecheck = underscore_join(uppercase_with_IS_CLASS)

                uppercase_with_H = map(lambda x: upper(x),
                                       [self.prefix] + self.name_elements + ['H'])
                self.includeguard = underscore_join(uppercase_with_H)

        regexp_match_uscore_element = re.compile('([A-Za-z0-9]+)')

        # GTK_CTREE_ doesn't contain the info that it should
        # be CTree instead of Ctree so we fix it.
        # since set_cast() is only used to set up enums
        # we can skip most of the cases that actually exist
        fix_caps_cases = { 'Ctree' : 'CTree',
                           'Clist' : 'CList',
                           '2button' : '2Button',
                           '3button' : '3Button',
                           'Ts' : 'TS',
                           'Gc' : 'GC',
                           'Ccallback' : 'CCallback'
                           }
                
        # set from GTK_BLAH_FOO
        def set_cast(self, cast, prefix_from = None):
                elems = []
                remainder = cast
                while remainder != "":
                        mo = self.regexp_match_uscore_element.match(remainder)

                        if mo != None:
                                elem = mo.group(0)
                                elem = capitalize(lower(elem))
                                if self.fix_caps_cases.has_key(elem):
                                        elem = self.fix_caps_cases[elem]
                                elems.append(elem)
                                remainder = mo.string[mo.end(0):]
                                # chop leading underscore
                                if remainder != "":
                                        remainder = remainder[1:]
                        else:
                                break

                basename = join(elems, '')

                self.set_basename(basename, prefix_from)

        # GtkBlahFoo
        def get_basename(self):
                return self.basename

        # BlahFoo
        def get_without_prefix(self):
                return self.no_prefix_name

        # Gtk
        def get_prefix(self):
                return self.prefix

        # gtk_blah_foo
        def get_function_prefix(self):
                return self.function_prefix

        # GTK_BLAH_FOO
        def get_cast(self):
                return self.cast

        # BLAH_FOO
        def get_cast_without_prefix(self):
                return self.no_prefix_cast

        # GtkBlahFooClass
        def get_classname(self):
                return self.classname

        # GTK_BLAH_FOO_CLASS
        def get_class_cast(self):
                return self.classcast

        # GTK_IS_BLAH_FOO
        def get_type_check(self):
                return self.typecheck

        # GTK_BLAH_FOO_TYPE
        def get_type_id(self):
                return self.typeid

        # GTK_IS_BLAH_FOO_CLASS
        def get_class_type_check(self):
                return self.classtypecheck

        # GTK_BLAH_FOO_H
        def get_include_guard(self):
                return self.includeguard

        # blah_foo
        def get_variable_name(self):
                return self.var_name

class Object:
        def __init__(self, name):
                self.nv = NameVariants()
                self.nv.set_basename(name)
                self.parent = None
                # list of struct fields we need accessors for
                self.fields = []
                # list of Function objects that are methods
                self.methods = []
                self.constructors = []
                self.static_members = []

        def get_nv(self):
                return self.nv

        def set_parent(self, obj):
                self.parent = obj

        def get_parent(self):
                return self.parent

        def set_fields(self, fields):
                self.fields = fields

        def get_fields(self):
                return self.fields

        def append_field(self, type, name):
                self.fields.append((type, name))

        def append_method(self, method):
                self.methods.append(method)

        def get_methods(self):
                return self.methods

        def append_constructor(self, constructor):
                self.constructors.append(constructor)

        def get_constructors(self):
                return self.constructors

        def get_static_members(self):
                return self.static_members

        def append_static_member(self, m):
                self.static_members.append(m)

class Function:
        def __init__(self, name, rettype, args):
                self.name = name
                self.args = args
                self.rettype = rettype
                self.method_of = None
                self.constructor_of = None
                self.static_member_of = None

        def get_name(self):
                return self.name

        def get_args(self):
                return self.args

        def get_method_of(self):
                return self.method_of

        def set_method_of(self, obj):
                if self.method_of != None:
                        print "Warning: setting function %s as method of two objects" % self.name
                elif self.constructor_of != None:
                        print "Warning: setting function %s as method after also setting it as constructor" % self.name
                elif self.static_member_of != None:
                        print "Warning: setting function %s as method after also setting it as static member" % self.name
                self.method_of = obj

        def set_constructor_of(self, obj):
                if self.constructor_of != None:
                        print "Warning: setting function %s as constructor of two objects" % self.name
                elif self.method_of != None:
                        print "Warning: setting function %s as constructor after also setting it as method" % self.name
                elif self.static_member_of != None:
                        print "Warning: setting function %s as constructor after also setting it as static member" % self.name
                self.constructor_of = obj

        def get_constructor_of(self):
                return self.constructor_of

        def get_rettype(self):
                return self.rettype

        def is_not_member(self):
                return self.constructor_of == None and self.method_of == None and self.static_member_of == None

        def is_member(self):
                return not self.is_not_member()

        def get_static_member_of(self):
                return self.static_member_of

        def set_static_member_of(self, obj):
                if self.static_member_of != None:
                        print "Warning: setting function %s as static member of two objects" % self.name
                elif self.method_of != None:
                        print "Warning: setting function %s as static member after also setting it as method" % self.name
                elif self.constructor_of != None:
                        print "Warning: setting function %s as static member after also setting it as constructor" % self.name
                self.static_member_of = obj

class Macro (Function):
        def __init__(self, name, rettype, args):
                Function.__init__(self, name, rettype, args)

class EnumValue:
        dont_use_prefix_from_special_cases = { 'GTK_UPDATE_ALWAYS' : '',
                                               'GTK_UPDATE_IF_VALID' : '',
                                               'GTK_PROGRESS_LEFT_TO_RIGHT' : '',
                                               'GTK_PROGRESS_RIGHT_TO_LEFT' : '',
                                               'GTK_PROGRESS_BOTTOM_TO_TOP' : '',
                                               'GTK_PROGRESS_TOP_TO_BOTTOM' : '',
                                               'GTK_PROGRESS_CONTINUOUS' : '',
                                               'GTK_PROGRESS_DISCRETE' : ''
                                               }
        def __init__(self, name, enum):
                self.nv = NameVariants()
                if self.dont_use_prefix_from_special_cases.has_key(name):
                        self.nv.set_cast(name)
                else:
                        self.nv.set_cast(name, enum.get_nv().get_basename())

        def get_nv(self):
                return self.nv

class Enum:
        def __init__(self, name):
                self.nv = NameVariants()
                self.nv.set_basename(name)
                self.values = []

        def get_nv(self):
                return self.nv

        def append_value(self, v):
                self.values.append(v)

        def get_values(self):
                return self.values

class Boxed:
        def __init__(self, name):
                self.nv = NameVariants()
                self.nv.set_basename(name)

        def get_nv(self):
                return self.nv

class FlagValue:
        dont_use_prefix_from_special_cases = {'GTK_TOPLEVEL' : '',
                                              'GTK_NO_WINDOW' : '',
                                              'GTK_REALIZED' : '',
                                              'GTK_MAPPED' : '',
                                              'GTK_VISIBLE' : '',
                                              'GTK_SENSITIVE' : '',
                                              'GTK_PARENT_SENSITIVE' : '',
                                              'GTK_CAN_FOCUS' : '',
                                              'GTK_HAS_FOCUS' : '',
                                              'GTK_CAN_DEFAULT' : '',
                                              'GTK_HAS_DEFAULT' : '',
                                              'GTK_HAS_GRAB' : '',
                                              'GTK_RC_STYLE' : '',
                                              'GTK_COMPOSITE_CHILD' : '',
                                              'GTK_NO_REPARENT' : '',
                                              'GTK_APP_PAINTABLE' : '',
                                              'GTK_DESTROYED' : '',
                                              'GTK_FLOATING' : '',
                                              'GTK_CONNECTED' : '',
                                              'GTK_CONSTRUCTED' : '',
                                              'GTK_RECEIVES_DEFAULT' : ''
                                              }
        def __init__(self, name, enum):
                self.nv = NameVariants()
                if self.dont_use_prefix_from_special_cases.has_key(name):
                        self.nv.set_cast(name)
                else:
                        self.nv.set_cast(name, enum.get_nv().get_basename())

        def get_nv(self):
                return self.nv

class Flag:
        def __init__(self, name):
                self.nv = NameVariants()
                self.nv.set_basename(name)
                self.values = []

        def get_nv(self):
                return self.nv

        def append_value(self, v):
                self.values.append(v)

        def get_values(self):
                return self.values


class Typedef:
        def __init__(self, name):
                self.nv = NameVariants()
                self.nv.set_basename(name)

        def get_nv(self):
                return self.nv

class TypesParser(scmexpr.Parser):
	"""A parser that only parses definitions -- no output"""
	def define_enum(self, name, *values):
                if enums.has_key(name):
                        print "Warning: duplicate enum ", name
                else:
                        e = Enum(name)
                        enums[name] = e
                        for v in values:
                                vobj = EnumValue(v[1], e)
                                e.append_value(vobj)

	def define_flags(self, name, *values):
                if flags.has_key(name):
                        print "Warning: duplicate flag ", name
                else:
                        e = Flag(name)
                        flags[name] = e
                        for v in values:
                                vobj = FlagValue(v[1], e)
                                e.append_value(vobj)
                
	def define_object(self, name, parents, *args):                
                o = None
                if objects.has_key(name):
                        o = objects[name]
                else:
                        o = Object(name)
                        objects[name] = o
                        objects_by_prefix[o.get_nv().get_function_prefix()] = o

                # for now, always one parent
                pname = parents[0]

                p = None
                if objects.has_key(pname):
                        p = objects[pname]
                elif pname == 'nil':
                        pass # GtkObject has no parent
                else:
                        p = Object(pname)
                        objects[pname] = p
                        objects_by_prefix[p.get_nv().get_function_prefix()] = p

                if p != None:
                        o.set_parent(p)
                else:
                        if name != 'GtkObject':
                                print "Warning: only GtkObject may have no parent class: ", name

                for arg in args:
                        if arg != () and arg[0] == 'fields':
                                for field in arg[1:]:
                                        o.append_field(field[0], field[1])

        def define_macro(self, name, rettype, args):
                m = Macro(name, rettype, args)
                macros[name] = m
                
	def define_boxed(self, name, reffunc=None, unreffunc=None, size=None):
                b = Boxed(name)
                boxeds[name] = b

	def define_func(self, name, rettype, args):
                for a in args:
                        if a[0] == 'varargs':
                                print "Warning: function %s has varargs, not wrapping" % name
                                return

                # These two conflict with the copy constructor in C++
                # and are just convenience functions anyway
                if name == 'gtk_radio_button_new_with_label_from_widget':
                        return
                elif name == 'gtk_radio_button_new_from_widget':
                        return
                        
                f = Function(name, rettype, args)
                functions[name] = f

        def define_type_alias(self, origname, name):
                td = Typedef(name)
                typedefs[name] = td

        def define_func_type(self, name, rettype, args):
                td = Typedef(name)
                typedefs[name] = td

	def include(self, filename):
		if filename[0] != '/':
			# filename relative to file being parsed
			afile = os.path.join(os.path.dirname(self.filename),
					     filename)
		else:
			afile = filename
		if not os.path.exists(afile):
			# fallback on PWD relative filename
			afile = filename
		#print "including file", afile
		fp = open(afile)
		self.startParsing(scmexpr.parse(fp))

        def associate_methods(self):
                for (name, func) in functions.items():
                        args = func.get_args()
                        if args != () and objects.has_key(args[0][0]):
                                obj = objects[args[0][0]]
                                fprefix = obj.get_nv().get_function_prefix()
                                if name[0:len(fprefix)] == fprefix:
                                        func.set_method_of(obj)
                                        obj.append_method(func)
                                # special case the gtk_signal_* functions that
                                # take a GtkObject as first arg
                                elif name[0:len('gtk_signal')] == 'gtk_signal' and \
                                     obj.get_nv().get_basename() == 'GtkObject':
                                        func.set_method_of(obj)
                                        obj.append_method(func)

                        # check for constructor
                        # deliberately checked even if we found a method,
                        # so we can ensure our checking methods
                        # don't generate overlaps
                        where = find(name, '_new')
                        if (where >= 0):
                                prefix = name[:where]
                                if objects_by_prefix.has_key(prefix):
                                        obj = objects_by_prefix[prefix]
                                        func.set_constructor_of(obj)
                                        obj.append_constructor(func)
                                else:
                                        print "Warning: no object for prefix %s on constructor %s" % (prefix, func.get_name())

                # after assigning all methods and constructors, check
                # for static member for all remaining functions
                
                for (name, func) in functions.items():
                        if func.get_constructor_of() == None and func.get_method_of() == None:
                                # check long prefixes first, otherwise
                                # gtk_button_box/gtk_button, etc. will hose us
                                
                                elems = split(name, '_')
                                for i in range(len(elems), 0, -1):
                                        prefix = join(elems[0:i], '_')
                                        if objects_by_prefix.has_key(prefix):
                                                obj = objects_by_prefix[prefix]
                                                func.set_static_member_of(obj)
                                                obj.append_static_member(func)
                                                break

                # Now do macros; here we assume they can only be methods,
                # clearly they aren't constructors and I can't
                # think of a static member example
                # However checking them is a bit different
                # since the names are in caps
                for (name, macro) in macros.items():
                        args = macro.get_args()
                        if args != () and objects.has_key(args[0][0]):
                                obj = objects[args[0][0]]
                                fprefix = obj.get_nv().get_function_prefix()
                                if name[0:len(fprefix)] == upper(fprefix):
                                        macro.set_method_of(obj)
                                        obj.append_method(macro)
                                
class FunctionDefsParser(TypesParser):
	def __init__(self, input, prefix):
		TypesParser.__init__(self, input)

	def unknown(self, tup):
		print "Unknown scheme function:", (tup and tup[0] or "")

	def define_object(self, name, parent=(), fields=()):
                TypesParser.define_object(self, name, parent, fields)
        
	def define_func(self, name, retType, args):
                TypesParser.define_func(self, name, retType, args)

class FilteringParser(FunctionDefsParser):
        """Parser that filters stuff out"""
        def __init__(self, input, prefix):
		FunctionDefsParser.__init__(self, input, prefix)



